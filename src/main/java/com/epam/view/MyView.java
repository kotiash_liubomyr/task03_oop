package com.epam.view;

import com.epam.controller.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - print list of electrical appliance");
        menu.put("2", "  2 - add electrical appliance to list");
        menu.put("3", "  3 - plug/unplug the appliance into an outlet");
        menu.put("4", "  4 - on/off electrical appliance");
        menu.put("5", "  5 - add operating mode");
        menu.put("6", "  6 - change operating mode");
        menu.put("7", "  7 - calculate power");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
    }

    private void pressButton1() {
        System.out.println(controller.getListOfElectricalAppliance());
    }

    private void pressButton2() {
        System.out.println("Input type of electrical appliance");
        String type = input.nextLine();
        System.out.println("Input name of electrical appliance");
        String name = input.nextLine();
        System.out.println("Input power of electrical appliance");
        double power = input.nextDouble();
        if(controller.add(type, name, power)) {
            System.out.println("Success");
        } else {
            System.out.println("Failure");
        }
    }

    private void pressButton3() {
        System.out.println("Input name of electrical appliance");
        String name = input.nextLine();
        controller.plug(name);
    }

    private void pressButton4() {
        System.out.println("Input name of electrical appliance");
        String name = input.nextLine();
        if(!controller.on(name)) {
            System.err.println("This electrical appliance is not switchable");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void pressButton5() {
        System.out.println("Input name of electrical appliance");
        String name = input.nextLine();
        System.out.println("Input name of mode of electrical appliance");
        String nameMode = input.nextLine();
        System.out.println("Input power of mode of electrical appliance");
        double powerMode = input.nextDouble();
        if(!controller.addOperatingMode(name, nameMode, powerMode)) {
            System.err.println("This electrical appliance has only one power mode");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void pressButton6() {
        System.out.println("Input name of electrical appliance");
        String name = input.nextLine();
        System.out.println("Input name of mode of electrical appliance");
        String nameMode = input.nextLine();
        if(!controller.changeOperatingMode(name, nameMode)) {
            System.err.println("This electrical appliance has only one power mode");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void pressButton7() {
        System.out.println(controller.calculatePower());
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            if (methodsMenu.containsKey(keyMenu)) {
                methodsMenu.get(keyMenu).print();
            }
        } while (!keyMenu.equals("Q"));
    }
}
