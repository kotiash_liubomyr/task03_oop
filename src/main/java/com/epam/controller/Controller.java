package com.epam.controller;

public interface Controller {
    String getListOfElectricalAppliance();
    boolean add(String type, String name, double power);
    public void plug(String name);
    public boolean on(String name);
    public boolean addOperatingMode(String name, String nameMode, double powerMode);
    public boolean changeOperatingMode(String name, String nameMode);
    public double calculatePower();
}
