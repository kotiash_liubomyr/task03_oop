package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

public class ControllerImpl implements Controller {
    Model model;
    public ControllerImpl() {
        model = new BusinessLogic();
    }
    public String getListOfElectricalAppliance() {
        return model.getListOfElectricalAppliance();
    }
    public boolean add(String type, String name, double power) {
        return model.add(type, name, power);
    }
    public void plug(String name) {
        model.plug(name);
    }
    public boolean on(String name) {
        return model.on(name);
    }
    public boolean addOperatingMode(String name, String nameMode, double powerMode) {
        return model.addOperatingMode(name, nameMode, powerMode);
    }
    public boolean changeOperatingMode(String name, String nameMode) {
        return model.changeOperatingMode(name, nameMode);
    }
    public double calculatePower() {
        return model.calculatePower();
    }
}
