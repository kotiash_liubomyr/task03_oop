package com.epam.model;

@FunctionalInterface
public interface GetElectricalAppliance {
    ElectricalAppliance get(double power);
}
