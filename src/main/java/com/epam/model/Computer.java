package com.epam.model;

public class Computer extends SwitchableElectricAppliance {

    public Computer(double power) {
        super(power);
    }
}
