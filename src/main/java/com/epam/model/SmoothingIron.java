package com.epam.model;

import java.util.LinkedHashMap;
import java.util.Map;

public class SmoothingIron extends ElectricalAppliance  implements ChangeOperatingMode {
    private String operationModeKey;
    private Map<String, Double> operationMode;
    SmoothingIron(double power) {
        super(power);
        this.operationMode = new LinkedHashMap<>();
    }

    @Override
    public double getPower() {
        if(operationMode == null) {
            return super.getPower();
        } else if (isSwitch) {
            return operationMode.get(operationModeKey);
        }
        return 0;
    }
    @Override
    public String changeOperatingMode(String nameMode) {
        if (operationMode.containsKey(nameMode)) {
            operationModeKey = nameMode;
            return operationModeKey + "successfully installed";
        } else {
            return nameMode + "does not exist";
        }
    }

    @Override
    public boolean addOperatingMode(String nameMode, double powerMode) {
        if (!operationMode.containsKey(nameMode)) {
            operationMode.put(nameMode, powerMode);
            return true;
        }
        return false;
    }
}
