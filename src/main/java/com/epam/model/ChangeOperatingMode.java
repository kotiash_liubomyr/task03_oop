package com.epam.model;

public interface ChangeOperatingMode {
    String changeOperatingMode(String nameMode);
    boolean addOperatingMode(String nameMode, double powerMode);
}
