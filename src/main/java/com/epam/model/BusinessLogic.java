package com.epam.model;

import java.util.Map;
import java.util.TreeMap;

public class BusinessLogic implements Model {
    private Map<String, ElectricalAppliance> electricalAppliances;
    private Map<String, GetElectricalAppliance> types;
    public BusinessLogic() {
        electricalAppliances = new TreeMap<>();
        types = new TreeMap<>();
        types.put("AirConditioning", this::getAirConditioning);
        types.put("Computer", this::getComputer);
        types.put("ElectricCooker", this::getElectricCooker);
        types.put("HairDryer", this::getHairDryer);
        types.put("Heater", this::getHeater);
        types.put("Kettle", this::getKettle);
        types.put("Lamp", this::getLamp);
        types.put("Monitor", this::getMonitor);
        types.put("MultiCooking", this::getMultiCooking);
        types.put("Refrigerator", this::getRefrigerator);
        types.put("SmoothingIron", this::getSmoothingIron);
        types.put("Toaster", this::getToaster);
        types.put("TV", this::getTV);
        types.put("Ventilator", this::getVentilator);
        types.put("WashingMachine", this::getWashingMachine);
    }

    public boolean add(String type, String name, double power) {
        if(types.containsKey(type)) {
            electricalAppliances.put(name, getLink(type, power));
            return true;
        }
        return false;
    }

    public String getListOfElectricalAppliance() {
        StringBuilder result = new StringBuilder();
        for(String s : electricalAppliances.keySet()) {
            result.append(s);
            result.append("\n");
        }
        return result.toString();
    }

    public void plug(String name) {
        if(electricalAppliances.get(name).isSwitch) {
            electricalAppliances.get(name).unplug();
        } else {
            electricalAppliances.get(name).plug();
        }
    }

    public boolean on(String name) {
        if(electricalAppliances.get(name) instanceof SwitchableElectricAppliance) {
            if (((SwitchableElectricAppliance) electricalAppliances.get(name)).isOn) {
                ((SwitchableElectricAppliance) electricalAppliances.get(name)).on();
            } else {
                ((SwitchableElectricAppliance) electricalAppliances.get(name)).off();
            }
            return true;
        } else {
            return false;
        }
    }

    public boolean addOperatingMode(String name, String nameMode, double powerMode) {
        if(electricalAppliances.get(name) instanceof ChangeOperatingMode) {
            ((ChangeOperatingMode) electricalAppliances.get(name)).addOperatingMode(nameMode, powerMode);
            return true;
        } else {
            return false;
        }
    }

    public boolean changeOperatingMode(String name, String nameMode) {
        if(electricalAppliances.get(name) instanceof ChangeOperatingMode) {
            ((ChangeOperatingMode) electricalAppliances.get(name)).changeOperatingMode(nameMode);
            return true;
        } else {
            return false;
        }
    }

    public double calculatePower() {
        double result = 0;
        for (ElectricalAppliance electricalAppliance : electricalAppliances.values()) {
            result += electricalAppliance.getPower();
        }
        return result;
    }

    private ElectricalAppliance getLink(String type, double power) {
        return types.get(type).get(power);
    }
    private ElectricalAppliance getAirConditioning(double power) {
        return new AirConditioning(power);
    }
    private ElectricalAppliance getComputer(double power) {
        return new Computer(power);
    }
    private ElectricalAppliance getElectricCooker(double power) {
        return new ElectricCooker(power);
    }
    private ElectricalAppliance getHairDryer(double power) {
        return new HairDryer(power);
    }
    private ElectricalAppliance getHeater(double power) {
        return new Heater(power);
    }
    private ElectricalAppliance getKettle(double power) {
        return new Kettle(power);
    }
    private ElectricalAppliance getLamp(double power) {
        return new Lamp(power);
    }
    private ElectricalAppliance getMonitor(double power) {
        return new Monitor(power);
    }
    private ElectricalAppliance getMultiCooking(double power) {
        return new MultiCooking(power);
    }
    private ElectricalAppliance getRefrigerator(double power) {
        return new Refrigerator(power);
    }
    private ElectricalAppliance getSmoothingIron(double power) {
        return new SmoothingIron(power);
    }
    private ElectricalAppliance getToaster(double power) {
        return new Toaster(power);
    }
    private ElectricalAppliance getTV(double power) {
        return new TV(power);
    }
    private ElectricalAppliance getVentilator(double power) {
        return new Ventilator(power);
    }
    private ElectricalAppliance getWashingMachine(double power) {
        return new WashingMachine(power);
    }
}
