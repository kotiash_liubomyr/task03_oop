package com.epam.model;

public abstract class SwitchableElectricAppliance extends ElectricalAppliance {
    protected boolean isOn = false;

    SwitchableElectricAppliance(double power) {
        super(power);
    }
    public double getPower() {
        if (isSwitch && isOn) {
            return power;
        }
        return 0;
    }
    public void on() {
        this.isOn = true;
    }
    public void off() {
        this.isOn = false;
    }
}
