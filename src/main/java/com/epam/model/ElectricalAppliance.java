package com.epam.model;

import com.sun.org.apache.xpath.internal.objects.XString;

public abstract class ElectricalAppliance {
    protected boolean isSwitch = false;
    protected double power;

    ElectricalAppliance(double power) {
        this.power = power;
    }

    public double getPower() {
        if (isSwitch) {
            return power;
        }
        return 0;
    }

    public double getMaxPower() {
        return power;
    }
    public void plug() {
        this.isSwitch = true;
    }

    public void unplug() {
        this.isSwitch = false;
    }
}
